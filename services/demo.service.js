const axios = require('axios');
const https = require('https');

const {
  KEYCLOAK_BASE_URL,
  KEYCLOAK_GRANT_TYPE,
  KEYCLOAK_CLIENT_ID,
  KEYCLOAK_CLIENT_SECRET,
} = process.env;

const client = axios.create({
  baseURL: KEYCLOAK_BASE_URL,
  httpsAgent: new https.Agent({ rejectUnauthorized: false }),
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
});

const validateUser = async (username, password) => {
  const authResponse = await client.post(
    '/realms/customers/protocol/openid-connect/token',
    {
      grant_type: KEYCLOAK_GRANT_TYPE,
      client_id: KEYCLOAK_CLIENT_ID,
      client_secret: KEYCLOAK_CLIENT_SECRET,
      username,
      password,
      scope: "openid"
    }
  );

  const userInfo = await client.get(
    '/realms/customers/protocol/openid-connect/userinfo',
    {
      headers: {
        Authorization: `Bearer ${authResponse.data.access_token}`,
      },
    }
  );

  return userInfo.data;
};

const runDemo = (username, password) => {
  return validateUser(username, password);
};

module.exports = { runDemo };
