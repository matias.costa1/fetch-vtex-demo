const supertest = require('supertest');
const { app, server } = require('../app');
const { demo } = require('../models/user.model');

const api = supertest(app);

test('Teste ping status code', async () => {
  await api.get('/ping').expect(200);
});

test('Teste ping response', async () => {
  const response = await api.get('/ping');
  expect(response.text).toBe('pong');
});

it('prueba demo', () => {
  expect(demo({ age: 20 })).toBe(true);
});

it('prueba demo', () => {
  expect(demo({ age: 10 })).toBe(false);
});

it('prueba demo', () => {
  expect(demo({ age: 18 })).toBe(false);
});

afterAll(() => {
  server.close();
});
