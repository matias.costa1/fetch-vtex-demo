const { runDemo } = require('../services/demo.service');

const demo = async (req, res) => {
  const { username, password } = req.headers;
  const result = await runDemo(username, password);
  res.json(result);
};

module.exports = { demo };
