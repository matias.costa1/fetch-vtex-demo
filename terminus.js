/* eslint-disable max-len */
/* istanbul ignore file */
const { createTerminus } = require('@godaddy/terminus');

// eslint-disable-next-line no-unused-vars
function healthCheck({ state }) {
  // `state.isShuttingDown` (boolean) shows whether the server is shutting down or not
  return Promise.resolve();
}
function terminus(app) {
  const options = {
    // health check options
    healthChecks: {
      '/healthcheck': healthCheck, // a function accepting a state and returning a promise indicating service health,
    },
    // both
    logger: (msg, err) => {
      console.log(`Error: ${err}msg: ${msg}`);
    }, // [optional] logger function to be called with errors. Example logger call: ('error happened during shutdown', error). See terminus.js for more details.
  };

  if (process.env.KUBERNETES) {
    // eslint-disable-next-line no-inner-declarations
    function beforeShutdown() {
      // given your readiness probes run every 5 second
      // may be worth using a bigger number so you won't
      // run into any race conditions
      return new Promise(resolve => {
        setTimeout(resolve, 5000);
      });
    }
    createTerminus(app, {
      beforeShutdown,
      useExit0: true,
    });
  } else {
    createTerminus(app, options);
  }
}

module.exports.terminus = terminus;
