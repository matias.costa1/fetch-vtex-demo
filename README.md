# STARTER DE NODE

###  CORRER EL PROYECTO 

NPM INSTALL
DOCKER RUN START DOCKER

### TEST

NPM RUN TEST

Corre el script "test": "cross-env NODE_ENV=test jest --verbose --detectOpenHandles, remover
la bandera de --detectOpenHandles si no se quisiese visualizar la información completa de los test
al momento de correrlos.

NPM RUN COVERAGE

Permite ver localmente el % de coverage que hay sobre nuestro proyecto.  Dentro de la carpeta coverage
se genere un documento html en el cual podemos ver sobre que líneas y cuales no pasa nuestro test.

NPM RUN TEST:MUTATION 

Correr los test en modo mutation. Obliga al programador a testear todos los casos de uso posible en un test. A mayor mutantes muertos, más fiable serán nuestros test. 

### LINTER

NPM RUN LINT 

Permite visualizar las correciones a realizar por el linter.

NPM RUN LINT:FIX

Arregla algunas correciones del código a nivel de linter

### MODO DESAROLLO 

NPM RUN DEV 

Corre el servicio con nodemon, escucha los cambios cuando desarrollamos hy refrescar el servicio.
