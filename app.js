const express = require('express');

const app = express();
const port = process.env.PORT || 3001;
const swaggerUi = require('swagger-ui-express');
const Prometheus = require('prom-client');
const promBundle = require('express-prom-bundle');
const { logger } = require('./winston');
const { terminus } = require('./terminus');
const { demo } = require('./controller/demo.controller');
const swaggerDocument = require('./swagger.json');

const counter = new Prometheus.Counter({
  name: 'my_metric_request_operations_total',
  help: 'The total number of processed requests',
});

const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'my_metric_request_duration_seconds',
  help: 'Histogram for the duration in seconds.',
  labelNames: ['endpoint'],
  // buckets for response time from 0.1ms to 500ms
  buckets: [0.3, 0.9, 1.6, 3, 5, 10],
});

const countByEndpoint = new Prometheus.Counter({
  name: 'my_metric_request_by_endpoint',
  help: 'Number of get requests.',
  labelNames: ['path'],
});

const countByStatusCode = new Prometheus.Counter({
  name: 'my_metric_response_status',
  help: 'Status of HTTP response',
  labelNames: ['status'],
});

terminus(app);

const metricsMiddleware = promBundle({
  includeMethod: true,
  includePath: true,
  includeStatusCode: true,
  includeUp: true,
  customLabels: {
    project_name: 'hello_world',
    project_type: 'test_metrics_labels',
  },
  promClient: {
    collectDefaultMetrics: {},
  },
});
// add the prometheus middleware to all routes
app.use(metricsMiddleware);

function middleware(req, res, next) {
  counter.inc();
  countByEndpoint.labels(req.path).inc();
  const now = new Date();
  next();
  const finish = new Date();
  const total = (finish - now) / 1000;
  httpRequestDurationMicroseconds.labels(req.path).observe(total);
  countByStatusCode.labels(res.statusCode).inc();
}

app.use(middleware);
app.use(
  '/health',
  require('express-healthcheck')({
    healthy() {
      return { status: 'OK' };
    },
  })
);

app.post('/demo', demo);

app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType);
  res.send(Prometheus.register.metrics());
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const server = app.listen(port, () => {
  logger.log('debug', 'Starting app ');
  console.log(`Server listening on port ${port}`);
});

module.exports = { app, server };
